import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class MultiThreadChatServer {

	private static ServerSocket serverSocket = null;
	private static Socket clientSocket = null;
	private static final int maxClients = 10;
	private static final clientThread[] threads = new clientThread[maxClients];
	
	public static void main(String[]args){
		int port = 4567;
		System.out.println("Starting up chat server on port " + port);
		
		try{
			serverSocket = new ServerSocket(port);
			
		}catch(IOException e){
			e.printStackTrace();
			
		}
		while(true){
			try{
				clientSocket = serverSocket.accept();
				int i = 0;
				for(i = 0; i < maxClients; i++){
					if(threads[i] == null){
						(threads[i] = new clientThread(clientSocket, threads)).start();
						break;
					}
				}
				if(i == maxClients){
					PrintStream os = new PrintStream(clientSocket.getOutputStream());
					os.println("Server busy. Try later");
					os.close();
					clientSocket.close();
				}
			}catch(IOException e){
				e.printStackTrace();
			}
		}
	}
}

class clientThread extends Thread{
	
	private Scanner in = null;
	private PrintStream out = null;
	private Socket clientSocket = null;
	private final clientThread[] threads;
	private int maxClientsCount;
	
	public clientThread(Socket clientSocket, clientThread[] threads){
		this.clientSocket = clientSocket;
		this.threads = threads;
		maxClientsCount = threads.length;
	}

	public void run(){
		
		try{
			in = new Scanner(clientSocket.getInputStream());
			out = new PrintStream(clientSocket.getOutputStream(), true);
			out.println("Enter your name");
			String name = in.nextLine().trim();
			out.println("Hello " + name + " welcome to our chat room!");
			out.println("To leave enter /quit on a new line");
			
			//Meddela alla andra att denna anv�ndare kommit in i rummet
			for(int i = 0; i < maxClientsCount; i++){
				if(threads[i] != null && threads[i] != this){
					threads[i].out.println("*** A new user " + name + " entered the room***");	
				}
				
				while(true){
					String line = in.nextLine();
					
					//Om denna rad startar med /quit 
					//S� m�ste det vara detta objekts
					//anv�ndare som skickat det
					if(line.toLowerCase().startsWith("/quit")){
						break;
					}
					
					//Skicka denna lista till alla anv�ndare
					for(int j = 0; j < maxClientsCount; j++){
						
						if(threads[j] != null && threads[j] != this){
							threads[j].out.println("<" + name + ">" + line);
						}
					}
				}//while
				for(int d = 0; d < maxClientsCount; d++){
					if(threads[d] != null && threads[d] != this){
						threads[d].out.println("*** the user " + name + " has left the room ***");
					}
				}
				//S�g hejd� till anv�ndare
				out.println("*** Bye " + name + " ***");
				
				for(int x = 0; x < maxClientsCount; x++){
					if(threads[x] == this){
						threads[x] = null;
					}
				}
				
				in.close();
				out.close();
				clientSocket.close();
			
			}	
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}